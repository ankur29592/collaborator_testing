package com.alticeusa.hsd.cos.utils;

import java.util.LinkedHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.alticeusa.hsd.cos.exception.InternalServerErrorException;

public class MetricsLoggingUtils {
	
	private static final Logger logger = LogManager.getLogger("MetricsLoggingUtils.class");
	
	public static String buildJSONStringFromMap(LinkedHashMap<String, Object> metricsKeyValuePairs){
		
		try{
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(metricsKeyValuePairs);
		}
		catch(Exception e){
			
			logger.error(" Error in Metrics Logging " ,e);
			throw new InternalServerErrorException();
		}
	}

}

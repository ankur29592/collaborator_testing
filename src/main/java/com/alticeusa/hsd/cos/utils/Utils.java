package com.alticeusa.hsd.cos.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.alticeusa.sdl.sigma.message.SigmaEvent;
import com.alticeusa.sdl.sigma.message.SigmaEventResponse;

public class Utils {
	
	private static final Logger logger = LogManager.getLogger(Utils.class);
	SigmaEventResponse stateSalesTaxLookupServiceSuccessResponse = new SigmaEventResponse();

	public static String convertJSONObjectToString(Object obj) throws JsonGenerationException, JsonMappingException, IOException{
		ObjectMapper objMapper = new ObjectMapper();
		String json = objMapper.writeValueAsString(obj);
		return json;
	}
	
	public static String getCurrentTimeStamp(String format){
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		Date now = new Date();
		String formattedDateTime = dateFormat.format(now);
		return formattedDateTime;
	}	


	public static boolean validateRequest(SigmaEvent request) {
		if(areAllRequiredFieldsPresent(request)){
			logger.info(" <<<<< Validation Success .... ");
			return true;			
		}			
		logger.info(" <<<<< Validation failed ... ");
		return false;
	}
	
	
	
	public static boolean errorValidation(SigmaEvent request){
		return true;
	}
	
	
	private static boolean areAllRequiredFieldsPresent(SigmaEvent request) {
		 if(StringFormatter.checkNullString(request)) {
			 logger.info(" <<<<< AllRequiredFieldsPresent ... ");
			 return true;
		 }
		 logger.info(" <<<<< Failed at AllRequiredFieldsPresent ... ");
		 return false;
	}
	
	
	
}

package com.alticeusa.hsd.cos.utils;

import java.text.SimpleDateFormat;

public class Constants {
	//For Metrics Logging
	public static final String API_NAME="HsdChangeOfService";
	public static final String API_VERSION = "api/v1";
		public final  SimpleDateFormat LOG_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	public static final String FAIL= "FAIL";
		public static final String ERR_400_CODE ="400";
		public static final boolean PRETTY_PRINT = StringFormatter.toBoolean("true");
		public static final String METRICS_LOGGING_COMMON_DATE_TIME_FORMAT = "YYYY-MM-dd'T'HH:mm:ss.SSS";
		public static final int RESPONSE_CODE_204=204;
		public static final int RESPONSE_CODE_200=200;
		public static final int RESPONSE_CODE_400=400;
		public static final int RESPONSE_CODE_401=401;
		public static final int RESPONSE_CODE_403=403;
		public static final int RESPONSE_CODE_405=405;
		public static final int RESPONSE_CODE_500=500;
		public static final String Authentication_failed = "Authentication failed";
		
		public static final String METHOD_NAME = "hsdChangeOfService";
		public static final String SUCCESS_CODE = "success";
		public static final String COMMON_API_NAME = "uowCommon";
		public static final String ON = "ON";
		public static final String OFF = "OFF";

		public static final String VAL_YES = "Y";
		public static final String VAL_NO = "N";

		public static final String SUCCESS = "SUCCESS";
		
		public static final String CREATE_ACTION = "CREATE";
		public static final String UPDATE_ACTION = "UPDATE";
		public static final String CANCEL_ACTION = "CANCEL";

		public static final String SUB_TYPE_R = "R";
		public static final String SUB_TYPE_C = "C";

		public static final String TO_PIC = "0352";
		public static final String TO_LPIC = "0352";

		public static final String PENDING_ACTIVATION = "PA";
		public static final String TN_TYPE_COMTEL = "COMTEL";
		public static final String PORT_FLAG_I = "I";

		public static final String LEC_LISTING_N = "N";
		public static final String DIR_RECORD_TYPE = "S";
		public static final String OMIT_ADDRESS_N = "N";
		public static final String DIR_TRANSACTION_IC = "IC";
		public static final String DIR_CLASS_OF_SERVICE_B = "B";
		public static final String SERVICE_CODE_PHONE = "PHONE";
		public static final String QUANTITY_0 = "0";
		public static final String QUANTITY_1 = "1";

		public static final String RTY_LML = "LML";
		public static final String TOA_R = "R";
		public static final String LTY_9 = "9";
		public static final String ALI_A1 = "A1";
		public static final String WORK_ORDER_TYPE_INSTALL = "1";
		public static final String TN_DEACTIVATION_REQUESTED ="VOICE LINE REMOVED";
		public static final String INVALID_EVENT_MSG_REMOVE_A_LINE = "Invalid eventMessage for RemoveALine";
		public static final String METRICS_LOGGING_COMMON_DATE_TIME_KEY= "Datetime";
		public static final String METRICS_LOGGING_COMMON_CURRENT_THREAD_KEY = "Thread";
		public static final String METRICS_LOGGING_COMMON_CONTEXT_NAME_KEY = "contextName";
		public static final String METRICS_LOGGING_COMMON_SUB_CONTEXT_NAME_KEY = "contextName";
		public static final String METRICS_LOGGING_REPEATER_LOG_ENTRY_TYPR = "LogEntryType";
		public static final String COMPLETE_ACTION="Activate";
		public static final String WO_TYPE_VALUE_THREE="3";
		public static final String SHD_VALUE_ONE="1";
		public static final String VALUE_TEN="10";
		public static final String METRICS_LOGGING_REQUEST_SOURCEAPP = "SourceApp";
		public static final String METRICS_LOGGING_REQUEST_FOOTPRINT = "FootPrint";
		public static final String METRICS_LOGGING_REQUEST_ZIPCODE = "ZipCode";
		public static final String METRICS_LOGGING_REQUEST_RATECODES = "RateCodes";	
		
		
		public static final String METRICS_LOGGING_COMMMON_SUCCESS_KEY = "Success";
		public static final String METRICS_LOGGING_COMMON_ERROR_CODE_KEY = "ErrorCode";
		public static final String METRICS_LOGGING_COMMON_ERROR_MESSAGE_KEY = "ErrorMessage";
		
		
		public static final String METRICS_LOGGING_COMMON_CLASS_NAME_KEY = "ClassName";
		public static final String METRICS_LOGGING_COMMON_CLASS_NAME_VALUE = "";
		public static final String METRICS_LOGGING_CLASS_NAME_VALUE = "com.alticeusa.hsd.cos";
		public static final String METRICS_LOGGING_COMMON_METHOD_NAME_KEY = "MethodName";

		public static final String EMPTY_JSON= "{}";
		
		public static final String METRICS_LOGGING_COMMON_ERROR_CODE="ErrorCode";
		public static final String METRICS_LOGGING_COMMON_ERROR_MESSAGE= "ErrorMessage";
		
		
		public static final String SUCCESS_MESSAGE = "TRUE";
		public static final String FAILURE_MESSAGE = "FALSE";
		public static final String SUCCESS_ERROR_CODE_200  = "" ;
		public static final int VALUE_ZERO =0;
		public static final int VALUE_ONE =1;
		public static final int VALUE_FIVE =5;
		public static final int VALUE_SIX =6;
		
		public static final int FAILURE_ERROR_CODE_500 = 500;
		public static final String FAILURE_ERROR_MESSAGE_400= " Invalid input fields entered";
		public static final String FAILURE_ERROR_MESSAGE_500 = " Internal Server Error / Unexpected Situation raised ";
		public static final String SERVICE_CHANGED = "SERVICE_CHANGED";
		
		public static final String CUST_TYPE_BUSINESS = "Business";
		public static final String CUST_TYPE_RESIDENTIAL = "Residential";
		public static final String CUST_TYPE_C = "C";
		public static final String CUST_TYPE_R = "R";
	}

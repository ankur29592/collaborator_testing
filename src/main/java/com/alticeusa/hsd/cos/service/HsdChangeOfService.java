package com.alticeusa.hsd.cos.service;

import java.io.StringReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.hsd.cos.bo.HsdChangeOfServiceBO;
import com.alticeusa.hsd.cos.exception.InternalServerErrorException;
import com.alticeusa.hsd.cos.exception.NoContentException;
import com.alticeusa.sdl.sigma.message.SigmaEvent;
import com.alticeusa.sdl.sigma.message.SigmaEventResponse;

/**
 * This is the main service class for hsdAddOccurrence Service.
 * 
 * @author Dhirenda Gautam
 * @version 1.0
 * @since 2017-10-31
 */

@Path("/api/v1")
public class HsdChangeOfService {

	private static final Logger logger = LogManager.getLogger(HsdChangeOfServiceBO.class);

	@POST
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public SigmaEventResponse hsdChangeOfService(String request) {

		final String METHOD_NAME = "hsdChangeOfServiceJSONResp";
		logger.info(">>> method :" + METHOD_NAME + " : Invoked API : hsdChangeOfService/api/v1");

		HsdChangeOfServiceBO changeOfServiceBO = new HsdChangeOfServiceBO();
		SigmaEventResponse sigmaEventResponse = new SigmaEventResponse();
		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(SigmaEvent.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(request);
			SigmaEvent sigmaEvent = (SigmaEvent) jaxbUnmarshaller.unmarshal(reader);

			sigmaEventResponse = changeOfServiceBO.hsdGetResponse(sigmaEvent);

		} catch (InternalServerErrorException e) {
			throw e;
		} catch (NoContentException e) {
			throw e;
		} catch (Exception ex) {
			logger.error("Exception occured while processing hsdChangeOfService request exception:", ex);
			throw new InternalServerErrorException();
		}
		return sigmaEventResponse;
	}
}

package com.alticeusa.hsd.cos.bo;

import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.hsd.cos.metrics.HsdChangeOfServiceLoggingData;
import com.alticeusa.hsd.cos.data.GetCustomerInfo;
import com.alticeusa.hsd.cos.data.GetSigmaHSDDetails;
import com.alticeusa.hsd.cos.data.GetSigmaHSDDetailsResponse;
import com.alticeusa.hsd.cos.data.GetTranNumberResponse;
import com.alticeusa.hsd.cos.service.HsdChangeOfService;
import com.alticeusa.hsd.cos.utils.Constants;
import com.alticeusa.hsd.cos.utils.PropertiesReader;
import com.alticeusa.sdl.sigma.message.AssocOrder;
import com.alticeusa.sdl.sigma.message.HSDCustomerInfo;
import com.alticeusa.sdl.sigma.message.HSDOccur;
import com.alticeusa.sdl.sigma.message.HSDOrder;
import com.alticeusa.sdl.sigma.message.Services;
import com.alticeusa.sdl.sigma.message.SigmaAction;
import com.alticeusa.sdl.sigma.message.SigmaEvent;
import com.alticeusa.sdl.sigma.message.SigmaEventResponse;

/**
 * This class is for generating building SigmaEventResponse Object.
 * 
 * @author Dhirenda Gautam
 * @version 1.0
 * @since 2017-10-31
 */

public class HsdChangeOfServiceBO {
	public String sigmaURL = "";
	public long transactionId;
	
	private static PropertiesReader prop = PropertiesReader.getInstance();
	public SigmaEventResponse sigmaEventResponse;
	public SigmaSPRestCalls sigmaSPRestCalls;
	private GetTranNumberResponse getTransactionId;
	private static final Logger logger = LogManager.getLogger(HsdChangeOfService.class);
  
	public void initiateResponseObject() {
		
		sigmaEventResponse = new SigmaEventResponse();
		ArrayList<SigmaAction> sigmaActionsArray = new ArrayList<SigmaAction>();
		sigmaEventResponse.setSigmaActions(sigmaActionsArray);
		sigmaSPRestCalls = new SigmaSPRestCalls();
		getTransactionId = new GetTranNumberResponse();
	}

	/**
	 * This method is control method for this BO. All the functions in the flow
	 * will be called by this method.
	 * 
	 * @param sigmaEvent
	 *            Enrichment Request Input
	 * @return a populated SigmaEventResponse instance
	 */

	public SigmaEventResponse hsdGetResponse(SigmaEvent sigmaEvent)  {
		initiateResponseObject();
		generateOrderEntities(sigmaEvent, Constants.COMPLETE_ACTION);
		performMetricsLogging(sigmaEvent, sigmaEventResponse);
		return sigmaEventResponse;
	}

	/**
	 * This method is for generating HSDOrder Object based of Provisioning
	 * Action.
	 * 
	 * @param sigmaEvent
	 *            Enrichment Request Input
	 * @param provisioningAction
	 *            Either ACTIVATE or DEACTIVATE
	 */

	public void generateOrderEntities(SigmaEvent sigmaEvent, String provisioningAction) {
		logger.info(">>>>In generateOrderEntities() ::: Generate HSDOrder for " + provisioningAction);
		GetSigmaHSDDetailsResponse getSigmaHSDDetailsResp=null;
		GetCustomerInfo getCustInfo= null;
		long sigmaEventIdVal = sigmaEvent.getSigmaEventId();
		String accountnumber = sigmaEvent.getAccountNumber();

		 getCustInfo = sigmaSPRestCalls.getCustInfo(sigmaEventIdVal, accountnumber);
		getTransactionId = sigmaSPRestCalls.getTransactionID();
		 getSigmaHSDDetailsResp = sigmaSPRestCalls.getSigmaHsdDet(sigmaEventIdVal);

		
		ArrayList<HSDOccur> hsdOccurList = new ArrayList<HSDOccur>();
				
		for (GetSigmaHSDDetails listItem : getSigmaHSDDetailsResp.getEventList()) {
			HSDOccur hsdOccur = new HSDOccur();
			ArrayList<Services> services = new ArrayList<Services>();
			Services serviceOccNew = new Services();
			Services serviceOccOld = new Services();
					
			if(Constants.SERVICE_CHANGED.equalsIgnoreCase(listItem.getStatus())){
				
				serviceOccNew.setFromQuantity(Constants.VALUE_ZERO);
				serviceOccNew.setToQuantity(Constants.VALUE_ONE);
				serviceOccNew.setServiceCode(listItem.getProductType());
				
				serviceOccOld.setFromQuantity(Constants.VALUE_ONE);
				serviceOccOld.setToQuantity(Constants.VALUE_ZERO);
				serviceOccOld.setServiceCode(listItem.getOldProductType());
				
				hsdOccur.setServiceOccurance(listItem.getOccurenceNumber());
				
				services.add(serviceOccOld);
				services.add(serviceOccNew);
				
				hsdOccur.setServices(services);
				
				hsdOccurList.add(hsdOccur);				
				
			}
			else {
				serviceOccNew.setFromQuantity(Constants.VALUE_ONE);
				serviceOccNew.setToQuantity(Constants.VALUE_ONE);
				serviceOccNew.setServiceCode(listItem.getProductType());
				
				hsdOccur.setServiceOccurance(listItem.getOccurenceNumber());
				services.add(serviceOccNew);
				hsdOccur.setServices(services);
				hsdOccurList.add(hsdOccur);	
			}
		}
		
		ArrayList<AssocOrder> assocOrderList = new ArrayList<AssocOrder>();
		AssocOrder assocOrder = new AssocOrder();
		assocOrder.setAssocAccountNumber(Constants.VALUE_ZERO);
		assocOrder.setAssocWorkOrder(Constants.VALUE_ZERO);
		assocOrderList.add(assocOrder);

		HSDCustomerInfo hsdCustomerInfo = new HSDCustomerInfo();
		hsdCustomerInfo.setAccountNumber(getCustInfo.getAccountNumber());
		hsdCustomerInfo.setWorkOrderNumber(sigmaEvent.getSigmaEventId());
		hsdCustomerInfo.setAction(null);

		int length = String.valueOf(getCustInfo.getScheduledDate()).length();
		String schDate = "";

		if (length != Constants.VALUE_ZERO && length == Constants.VALUE_FIVE) {
			schDate = Constants.VALUE_TEN + getCustInfo.getScheduledDate();
			hsdCustomerInfo.setScheduleDate(Long.parseLong(schDate));
		} else if (length == Constants.VALUE_SIX) {
			schDate = Constants.SHD_VALUE_ONE + getCustInfo.getScheduledDate();
			hsdCustomerInfo.setScheduleDate(Long.parseLong(schDate));
		}
        
		hsdCustomerInfo.setwOType(Constants.WO_TYPE_VALUE_THREE);
		hsdCustomerInfo.setAddrLocation(getCustInfo.getStreetNumber());
		hsdCustomerInfo.setFraction("");
		hsdCustomerInfo.setPreDirectional("");
		hsdCustomerInfo.setStreet(getCustInfo.getStreetName());
		hsdCustomerInfo.setAddrCity(getCustInfo.getCity());
		hsdCustomerInfo.setAddrState(getCustInfo.getState());
		hsdCustomerInfo.setBuilding("");
		hsdCustomerInfo.setApartment(getCustInfo.getApartmentNumber());
		hsdCustomerInfo.setAddrPostDirectional("");
		hsdCustomerInfo.setAddrZip5(getCustInfo.getZip());
		hsdCustomerInfo.setHeadend(getCustInfo.getHeadend());
		hsdCustomerInfo.setNode(getCustInfo.getNode());
		hsdCustomerInfo.setTitle("");
		hsdCustomerInfo.setFirstName(getCustInfo.getFirstName());
		hsdCustomerInfo.setMiddleInitial("");
		hsdCustomerInfo.setLastName(getCustInfo.getLastName());
		hsdCustomerInfo.setCustomerName(getCustInfo.getFirstName() + " " + getCustInfo.getLastName());
		hsdCustomerInfo.setHomePhoneNumber(getCustInfo.getHomePhone());
		hsdCustomerInfo.setHsdOccur(hsdOccurList);
		hsdCustomerInfo.setAssocOrders(assocOrderList);

		HSDOrder hsdOrder = new HSDOrder();
		if (getCustInfo.getCustomerType() != null && Constants.CUST_TYPE_BUSINESS.equalsIgnoreCase(getCustInfo.getCustomerType())) {
			hsdOrder.setSubType(Constants.CUST_TYPE_C);
		} else if (getCustInfo.getCustomerType() != null
				&& Constants.CUST_TYPE_RESIDENTIAL.equalsIgnoreCase(getCustInfo.getCustomerType())) {
			hsdOrder.setSubType(Constants.CUST_TYPE_R);
		} else {
			hsdOrder.setSubType("");
		}
		hsdOrder.setTransactionId(getTransactionId.getNextTransactionNumber());
		hsdOrder.setProvisioningAction(provisioningAction);
		hsdOrder.setCustomerInfo(hsdCustomerInfo);

		ArrayList<SigmaAction> sigmaActionsArray = sigmaEventResponse.getSigmaActions();
		SigmaAction sigmaActionOrder = new SigmaAction();
		sigmaActionOrder.setUrl(prop.getValue("order.url"));
		sigmaActionOrder.setSigmaMessage(hsdOrder);

		sigmaActionsArray.add(sigmaActionOrder);
		sigmaEventResponse.setSigmaActions(sigmaActionsArray);
		logger.info("<<< generateOrderEntities() ::: Generate HSDOrder for " + provisioningAction);
	}
	 private void performMetricsLogging(SigmaEvent request , SigmaEventResponse response)
	  {
		
		  String xmlRequest="";
		  String xmlResponse="";
		  HsdChangeOfServiceLoggingData metricsData = new HsdChangeOfServiceLoggingData(request, response);
		  try {
		  JAXBContext jaxbContext = JAXBContext.newInstance(SigmaEvent.class);
		  Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		  // output pretty printed
		 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter stringRequest = new StringWriter();
			jaxbMarshaller.marshal(metricsData.getRequest(), stringRequest);
			xmlRequest = stringRequest.toString();
		  
		  } catch (PropertyException e) {
				logger.error("Exception occured while setting property for jaxbMarshaller in performMetricsLogging Method  :", e);
		  } catch (JAXBException e) {
			    logger.error("JAXBException occured for jaxbMarshaller in performMetricsLogging Method:", e);
		  }
		  
		  try {
			  JAXBContext jaxbContext = JAXBContext.newInstance(SigmaEventResponse.class);
			  Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				StringWriter stringResponse = new StringWriter();
				jaxbMarshaller.marshal(metricsData.getResponse(), stringResponse);
				xmlResponse = stringResponse.toString();
			  
			  } catch (PropertyException e) {
					logger.error("Exception occured while setting property for jaxbMarshaller in performMetricsLogging Method  :", e);
			  } catch (JAXBException e) {
				    logger.error("JAXBException occured for jaxbMarshaller in performMetricsLogging Method:", e);
			 }
		  
		  
		  logger.info("Request for hsdChangeOfService:"+xmlRequest);
		  logger.info("Response for hsdChangeOfService:"+xmlResponse);
	  }
	
}
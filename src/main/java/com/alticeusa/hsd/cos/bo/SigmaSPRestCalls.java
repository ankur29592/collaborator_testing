package com.alticeusa.hsd.cos.bo;

import javax.ws.rs.core.UriBuilder;

import com.alticeusa.hsd.cos.data.GetCustomerInfo;
import com.alticeusa.hsd.cos.data.GetSigmaHSDDetailsResponse;
import com.alticeusa.hsd.cos.data.GetTranNumberResponse;
import com.alticeusa.hsd.cos.exception.InternalServerErrorException;
import com.alticeusa.hsd.cos.exception.NoContentException;
import com.alticeusa.hsd.cos.utils.Constants;
import com.alticeusa.hsd.cos.utils.PropertiesReader;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import sun.misc.BASE64Encoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class is for calling the Stored Procedure Services.
 * 
 * @author Dhirenda Gautam
 * @version 1.0
 * @since 2017-10-31
 */

public class SigmaSPRestCalls {

	public String custInfoURL = "";
	public String sigmaURL = "";
	public String modemURL = "";
	public String transURL = "";
	

	private static PropertiesReader prop = PropertiesReader.getInstance();

	public String authString = prop.getValue("application.user") + ":" + prop.getValue("application.pass");
	public String authStringEnc = new BASE64Encoder().encode(authString.getBytes());
	
	private static final Logger logger = LogManager.getLogger(SigmaSPRestCalls.class);
	
	Client client = Client.create();

	/**
	 * This method call the getCustomerInfo Service and returns the GetCustomerInfo Object.
	 * @param accountNumber
	 *            DDP Account Number
	 * @param sigmaEventIdVal
	 *            occurrence of this modem on the specified Account
	 * @return a populated GetCustomerInfo instance
	 */
	public GetCustomerInfo getCustInfo(long sigmaEventIdVal, String accountNumber)  {

		ClientResponse response= null;

		custInfoURL = new StringBuilder(prop.getValue("getCustomerInfo.url")).append(accountNumber)
				.append("/sigmaEventId/").append(sigmaEventIdVal).toString();
		
		logger.info(">>>>>>>>>Call Begin HSDGetCustInfo::URL:: "+ custInfoURL);
		
		try {
		
		WebResource serviceCustInfo = client.resource(UriBuilder.fromUri(custInfoURL).build());

		response = serviceCustInfo.header("Authorization", "Basic " + authStringEnc)
				.get(ClientResponse.class);
		}
		catch(Exception e)
		{
			logger.error("Exception occured while processing GetCustInfo request exception:", e);
			throw new InternalServerErrorException();		
		}
		logger.info("HTTPResponse Code From GetCustomerInfo Service:" + response.getStatus());
		logger.info("HTTPResponse Message From GetCustomerInfo Service:" + response.getStatusInfo().getReasonPhrase());
        
		if (response.getStatus() == Constants.RESPONSE_CODE_204) {
			logger.info("Null Message Body after call to GetCustomerInfo Service");
			throw new NoContentException();
		}

		if (response.getStatus() != Constants.RESPONSE_CODE_200) {
			logger.error("ERROR during call to GetCustomerInfo Service. Status:" + response.getStatus());
			throw new InternalServerErrorException();
		}

		GetCustomerInfo getCustInfo = response.getEntity(GetCustomerInfo.class);
		
		logger.info("<<<<<<<<Finished API execution for HSDGetCustInfo");

		return getCustInfo;
	}

	/**
	 * This method call the getSigmaDetails Service and returns the GetSigmaHSDDetailsResponse Object.
	 * @param sigmaEventIdVal
	 *            occurrence of this modem on the specified Account
	 * @return a populated GetSigmaHSDDetailsResponse instance
	 */
	public GetSigmaHSDDetailsResponse getSigmaHsdDet(long sigmaEventIdVal) {
		
		ClientResponse response= null;
		sigmaURL = new StringBuilder(prop.getValue("getSigmaDetails.url")).append(sigmaEventIdVal).toString();
		
		logger.info(">>>>>>>>>Call Begin HSDgetSigmaDetails::URL:: "+ sigmaURL);
		
		try
		{
	
		WebResource serviceSigma = client.resource(UriBuilder.fromUri(sigmaURL).build());
		
		response = serviceSigma.header("Authorization", "Basic " + authStringEnc)
				.get(ClientResponse.class);
		}
		catch (Exception e) {
			logger.error("Exception occured while processing HSDGetSigmaDetails request exception:", e);
			throw new InternalServerErrorException();
		}
		logger.info("HTTPResponse Code From GetSigmaHSDDetailsResponse Service:" + response.getStatus());
		logger.info("HTTPResponse Message From GetSigmaHSDDetailsResponse Service:" + response.getStatusInfo().getReasonPhrase());

		if (response.getStatus() == Constants.RESPONSE_CODE_204) {
			logger.info("Null Message Body after call to GetSigmaHSDDetailsResponse Service");
			throw new NoContentException();
		}

		if (response.getStatus() != Constants.RESPONSE_CODE_200) {
			logger.error("ERROR during call to GetSigmaHSDDetailsResponse Service. Status:" + response.getStatus());
			throw new InternalServerErrorException();
		}
		
		GetSigmaHSDDetailsResponse getSigmaHSDDetailsResp = response.getEntity(GetSigmaHSDDetailsResponse.class);
		
		logger.info("<<<<<<<<Finished API execution for HSDgetSigmaDetails");
		
		return getSigmaHSDDetailsResp;
	}

	/**
	 * This method call the getTransactionID Service and returns the Transaction ID.
	 * 
	 * @return transactionID
	 */
	public GetTranNumberResponse getTransactionID() {
		ClientResponse response = null;

		logger.info(">>>>> Call GetTransactionID::URL::" + prop.getValue("getTransactionID.url"));

		try {
			WebResource serviceTransID = client
					.resource(UriBuilder.fromUri(prop.getValue("getTransactionID.url")).build());

			response = serviceTransID.header("Authorization", "Basic " + authStringEnc).get(ClientResponse.class);
		}
		catch (Exception e) {
			logger.error("Exception occured while processing getTransactionID request exception:", e);
			throw new InternalServerErrorException();
		}

			logger.info("HTTPResponse Code From GetTranNumberResponse Service:" + response.getStatus());
			logger.info("HTTPResponse Message From GetTranNumberResponse Service:"
					+ response.getStatusInfo().getReasonPhrase());

			if (response.getStatus() == Constants.RESPONSE_CODE_204) {
				logger.info("Null Message Body after call to GetTranNumberResponse Service");
				throw new NoContentException();
			}

			if (response.getStatus() != Constants.RESPONSE_CODE_200) {
				logger.error("ERROR during call to GetTransactionID Service. Status:" + response.getStatus());
				throw new InternalServerErrorException();
			}

		GetTranNumberResponse transactionID = response.getEntity(GetTranNumberResponse.class);

		logger.info("<<<<<< Finished API Execution : GetTransactionID");
		return transactionID;
	}

}

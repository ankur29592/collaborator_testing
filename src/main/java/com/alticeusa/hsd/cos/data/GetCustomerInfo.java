package com.alticeusa.hsd.cos.data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is a pojo class that contains the structure of the customer Info output
 * 
 * @author Dhirenda Gautam
 * @version 1.0
 * @since 2017-10-31
 */
@XmlRootElement
public class GetCustomerInfo {

	private String accountNumber;
	private String firstName;
	private String lastName;
	private String middleInitials;
	private String apartmentPrefix;
	private String apartmentNumber;
	private String streetNumber;
	private String streetName;
	private String city;
	private String state;
	private String zip;
	private String fraction;
	private long scheduledDate;
	private String customerType;
	private String homePhone;
	private String homeAreaCode;
	private String headend;
	private String node;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleInitials() {
		return middleInitials;
	}

	public void setMiddleInitials(String middleInitials) {
		this.middleInitials = middleInitials;
	}

	public String getApartmentPrefix() {
		return apartmentPrefix;
	}

	public void setApartmentPrefix(String apartmentPrefix) {
		this.apartmentPrefix = apartmentPrefix;
	}

	public String getApartmentNumber() {
		return apartmentNumber;
	}

	public void setApartmentNumber(String apartmentNumber) {
		this.apartmentNumber = apartmentNumber;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getFraction() {
		return fraction;
	}

	public void setFraction(String fraction) {
		this.fraction = fraction;
	}

	public long getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(long scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getHomeAreaCode() {
		return homeAreaCode;
	}

	public void setHomeAreaCode(String homeareaCode) {
		this.homeAreaCode = homeareaCode;
	}

	public String getHeadend() {
		return headend;
	}

	public void setHeadend(String headend) {
		this.headend = headend;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public GetCustomerInfo(String accountNumber, String firstName, String lastName, String middleInitials,
			String apartmentPrefix, String apartmentNumber, String streetNumber, String streetName, String city,
			String state, String zip, String fraction, long scheduledDate, String customerType, String homePhone,
			String homeareaCode, String headend, String node) {
		super();
		this.accountNumber = accountNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleInitials = middleInitials;
		this.apartmentPrefix = apartmentPrefix;
		this.apartmentNumber = apartmentNumber;
		this.streetNumber = streetNumber;
		this.streetName = streetName;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.fraction = fraction;
		this.scheduledDate = scheduledDate;
		this.customerType = customerType;
		this.homePhone = homePhone;
		this.homeAreaCode = homeareaCode;
		this.headend = headend;
		this.node = node;
	}

	public GetCustomerInfo() {
		super();

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
		result = prime * result + ((apartmentNumber == null) ? 0 : apartmentNumber.hashCode());
		result = prime * result + ((apartmentPrefix == null) ? 0 : apartmentPrefix.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((customerType == null) ? 0 : customerType.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((fraction == null) ? 0 : fraction.hashCode());
		result = prime * result + ((headend == null) ? 0 : headend.hashCode());
		result = prime * result + ((homeAreaCode == null) ? 0 : homeAreaCode.hashCode());
		result = prime * result + ((homePhone == null) ? 0 : homePhone.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((middleInitials == null) ? 0 : middleInitials.hashCode());
		result = prime * result + ((node == null) ? 0 : node.hashCode());
		result = prime * result + (int) (scheduledDate ^ (scheduledDate >>> 32));
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((streetName == null) ? 0 : streetName.hashCode());
		result = prime * result + ((streetNumber == null) ? 0 : streetNumber.hashCode());
		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetCustomerInfo other = (GetCustomerInfo) obj;
		if (accountNumber == null) {
			if (other.accountNumber != null)
				return false;
		} else if (!accountNumber.equals(other.accountNumber))
			return false;
		if (apartmentNumber == null) {
			if (other.apartmentNumber != null)
				return false;
		} else if (!apartmentNumber.equals(other.apartmentNumber))
			return false;
		if (apartmentPrefix == null) {
			if (other.apartmentPrefix != null)
				return false;
		} else if (!apartmentPrefix.equals(other.apartmentPrefix))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (customerType == null) {
			if (other.customerType != null)
				return false;
		} else if (!customerType.equals(other.customerType))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (fraction == null) {
			if (other.fraction != null)
				return false;
		} else if (!fraction.equals(other.fraction))
			return false;
		if (headend == null) {
			if (other.headend != null)
				return false;
		} else if (!headend.equals(other.headend))
			return false;
		if (homeAreaCode == null) {
			if (other.homeAreaCode != null)
				return false;
		} else if (!homeAreaCode.equals(other.homeAreaCode))
			return false;
		if (homePhone == null) {
			if (other.homePhone != null)
				return false;
		} else if (!homePhone.equals(other.homePhone))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (middleInitials == null) {
			if (other.middleInitials != null)
				return false;
		} else if (!middleInitials.equals(other.middleInitials))
			return false;
		if (node == null) {
			if (other.node != null)
				return false;
		} else if (!node.equals(other.node))
			return false;
		if (scheduledDate != other.scheduledDate)
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (streetName == null) {
			if (other.streetName != null)
				return false;
		} else if (!streetName.equals(other.streetName))
			return false;
		if (streetNumber == null) {
			if (other.streetNumber != null)
				return false;
		} else if (!streetNumber.equals(other.streetNumber))
			return false;
		if (zip == null) {
			if (other.zip != null)
				return false;
		} else if (!zip.equals(other.zip))
			return false;
		return true;
	}
	
	
	
	

}

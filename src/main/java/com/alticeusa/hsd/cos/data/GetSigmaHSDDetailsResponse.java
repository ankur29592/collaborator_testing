package com.alticeusa.hsd.cos.data;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Dhirenda Gautam
 * @version 1.0
 * @since 2017-10-31
 */

@XmlRootElement
public class GetSigmaHSDDetailsResponse {

	// @JsonProperty("EVENT_LIST")
	private List<GetSigmaHSDDetails> eventList;

	@XmlElement(name = "EVENT_LIST")
	public List<GetSigmaHSDDetails> getEventList() {
		return eventList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eventList == null) ? 0 : eventList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetSigmaHSDDetailsResponse other = (GetSigmaHSDDetailsResponse) obj;
		if (eventList == null) {
			if (other.eventList != null)
				return false;
		} else if (!eventList.equals(other.eventList))
			return false;
		return true;
	}

	public void setEventList(List<GetSigmaHSDDetails> eventList) {
		this.eventList = eventList;
	}

}

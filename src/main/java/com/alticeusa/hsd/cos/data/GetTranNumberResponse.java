package com.alticeusa.hsd.cos.data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Dhirenda Gautam
 * @version 1.0
 * @since 2017-10-31
 */
@XmlRootElement
public class GetTranNumberResponse {
	private Long NextTransactionNumber;

	public Long getNextTransactionNumber() {
		return NextTransactionNumber;
	}

	public void setNextTransactionNumber(Long nextTransactionNumber) {
		NextTransactionNumber = nextTransactionNumber;
	}

}

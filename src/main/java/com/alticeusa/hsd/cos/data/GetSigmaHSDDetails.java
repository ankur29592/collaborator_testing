package com.alticeusa.hsd.cos.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This is POJO class for GetSigmaHSDDetails class. And output is
 * GetSigmaHSDDetails object
 * 
 * @author Ayush Yadav
 * @version 1.0
 * @since 2017-10-27
 */

@XmlRootElement
@XmlType(propOrder = { "macAddress", "productType", "status", "occurenceNumber", "oldProductType", "errorCode",
		"errorMsg" })
public class GetSigmaHSDDetails {

	// @JsonProperty("MAC_ADDRESS")
	private String macAddress;
	// @JsonProperty("PRODUCT_TYPE")
	private String productType;
	// @JsonProperty("STATUS")
	private String status;
	// @JsonProperty("OCCURENCE_NUMBER")
	private int occurenceNumber;
	// @JsonProperty("OLD_PRODUCT_TYPE")
	private String oldProductType;

	private String errorCode;

	private String errorMsg;

	@XmlElement(name = "MAC_ADDRESS")
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@XmlElement(name = "PRODUCT_TYPE")
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@XmlElement(name = "STATUS")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@XmlElement(name = "OCCURENCE_NUMBER")
	public int getOccurenceNumber() {
		return occurenceNumber;
	}

	public void setOccurenceNumber(int occurenceNumber) {
		this.occurenceNumber = occurenceNumber;
	}

	@XmlElement(name = "OLD_PRODUCT_TYPE")
	public String getOldProductType() {
		return oldProductType;
	}

	public void setOldProductType(String oldProductType) {
		this.oldProductType = oldProductType;
	}

	@XmlElement(name = "ERROR_CODE")
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	@XmlElement(name = "ERROR_MSG")
	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public GetSigmaHSDDetails(String macAddress, String productType, String status, int occurenceNumber,
			String oldProductType, String errorCode, String errorMsg) {
		super();
		this.macAddress = macAddress;
		this.productType = productType;
		this.status = status;
		this.occurenceNumber = occurenceNumber;
		this.oldProductType = oldProductType;
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}

	public GetSigmaHSDDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((errorCode == null) ? 0 : errorCode.hashCode());
		result = prime * result + ((errorMsg == null) ? 0 : errorMsg.hashCode());
		result = prime * result + ((macAddress == null) ? 0 : macAddress.hashCode());
		result = prime * result + occurenceNumber;
		result = prime * result + ((oldProductType == null) ? 0 : oldProductType.hashCode());
		result = prime * result + ((productType == null) ? 0 : productType.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetSigmaHSDDetails other = (GetSigmaHSDDetails) obj;
		if (errorCode == null) {
			if (other.errorCode != null)
				return false;
		} else if (!errorCode.equals(other.errorCode))
			return false;
		if (errorMsg == null) {
			if (other.errorMsg != null)
				return false;
		} else if (!errorMsg.equals(other.errorMsg))
			return false;
		if (macAddress == null) {
			if (other.macAddress != null)
				return false;
		} else if (!macAddress.equals(other.macAddress))
			return false;
		if (occurenceNumber != other.occurenceNumber)
			return false;
		if (oldProductType == null) {
			if (other.oldProductType != null)
				return false;
		} else if (!oldProductType.equals(other.oldProductType))
			return false;
		if (productType == null) {
			if (other.productType != null)
				return false;
		} else if (!productType.equals(other.productType))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
	

}
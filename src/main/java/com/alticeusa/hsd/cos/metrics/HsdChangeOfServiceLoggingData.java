package com.alticeusa.hsd.cos.metrics;

import com.alticeusa.hsd.cos.utils.Utils;

import java.util.LinkedHashMap;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.alticeusa.sdl.sigma.message.SigmaEvent;
import com.alticeusa.sdl.sigma.message.SigmaEventResponse;
import com.alticeusa.hsd.cos.utils.Constants;
import com.alticeusa.hsd.cos.utils.StringFormatter;

public class HsdChangeOfServiceLoggingData {
	private static final Logger logger = LogManager.getLogger(HsdChangeOfServiceLoggingData.class);
	
	private final SigmaEvent request;
	private final SigmaEventResponse response;
	private final  String dateTimeStamp;
	
	public SigmaEvent getRequest() {
		return request;
	}

	public SigmaEventResponse getResponse() {
		return response;
	}
	
	public HsdChangeOfServiceLoggingData(SigmaEvent request, SigmaEventResponse response) {
		this.request = request;
		this.response = response;
		this.dateTimeStamp= Utils.getCurrentTimeStamp(Constants.METRICS_LOGGING_COMMON_DATE_TIME_FORMAT);

	}
	
	
	/*@Override
	public String toString() {
		return getLogMetricsJSONString();
	}
	
	
	private String getLogMetricsJSONString(){
		String metricsJSON= Constants.EMPTY_JSON;
		try{
			LinkedHashMap<String, Object> metricsKeyValuePairs = new LinkedHashMap<String , Object>();
			addHeaderKeyValuePairs(metricsKeyValuePairs);
			addRequestKeyValuePairs(metricsKeyValuePairs);
			//addResponseKeyValuePairs(metricsKeyValuePairs);			
			metricsJSON = MetricsLoggingUtils.buildJSONStringFromMap(metricsKeyValuePairs);
		}
		catch(Exception e){
			logger.info(" Exception in metrics logging " +e.getMessage());
		}
		 return metricsJSON;
	}  
	
	
	
		public void addHeaderKeyValuePairs(LinkedHashMap<String, Object> metricsKeyValuePairs){
		
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_COMMON_DATE_TIME_KEY,dateTimeStamp);
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_COMMON_CURRENT_THREAD_KEY, Thread.currentThread().getName());
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_COMMON_CONTEXT_NAME_KEY, Constants.METRICS_LOGGING_COMMON_CONTEXT_NAME_VALUE);
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_COMMON_SUB_CONTEXT_NAME_KEY, Constants.METRICS_LOGGING_COMMON_SUB_CONTEXT_VALUE);
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_COMMON_CLASS_NAME_KEY,Constants.METRICS_LOGGING_CLASS_NAME_VALUE);
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_COMMON_METHOD_NAME_KEY,Constants.METRICS_LOGGING_METHOD_NAME_VALUE);
		
	}
	
	
	public void addRequestKeyValuePairs(LinkedHashMap<String , Object> metricsKeyValuePairs){
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_REQUEST_ACCOUNT_NUMBER, request.getAccountNumber());
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_REQUEST_EVENT_MODULE_ID, request.getEventModuleId());
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_REQUEST_MESSAGE, request.getMessage());
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_REQUEST_SIGMA_EVENT_ID, request.getSigmaEventId());
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_REQUEST_ZOOM_ORDER_ID, request.getZoomOrderId());
		
	}
	
	
	private void addResponseKeyValuePairs(LinkedHashMap<String, Object> metricsKeyValuePairs){
		if(areResponseFieldsPresent()){
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_COMMON_STATE,response.state);
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_COMMON_ERROR_CODE,response.errorCode);
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_COMMON_ERROR_MESSAGE,response.errorMessage);
		metricsKeyValuePairs.put(Constants.METRICS_LOGGING_COMMON_TAXRATES,response.getTaxRatesList());
		}
	}
	private boolean areResponseFieldsPresent() {
	    if(StringFormatter.checkNullStringResponse(response.getSigmaActions()))
	    {
	    	return false;
	    } else {
	    	return true;
	    }
	}*/
}

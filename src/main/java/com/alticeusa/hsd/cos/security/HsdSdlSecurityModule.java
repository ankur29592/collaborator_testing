package com.alticeusa.hsd.cos.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import com.alticeusa.hsd.cos.utils.Constants;
import com.alticeusa.hsd.cos.utils.PropertiesReader;

public class HsdSdlSecurityModule  implements LoginModule  {

	private static final Logger logger = LogManager.getLogger(HsdSdlSecurityModule.class);
	private static PropertiesReader prop = PropertiesReader.getInstance();
	private CallbackHandler handler;
	private Subject subject;
	private JAASUserPrincipal userPrincipal;
	private JAASRolePrincipal rolePrincipal;
	private String login;
	private List<String> userGroups;
	
	@Override
	public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState,
			Map<String, ?> options) {
		handler = callbackHandler;
		this.subject = subject;		
	}

	@Override
	public boolean login() throws LoginException {
		boolean loginValid = true;
		Callback[] callbacks = new Callback[2];
		callbacks[0] = new NameCallback("login");
		callbacks[1] = new PasswordCallback("password", true);
		try {
			handler.handle(callbacks);
			String name = ((NameCallback) callbacks[0]).getName();
			String password = String.valueOf(((PasswordCallback) callbacks[1]).getPassword());
			if (name != null
					&& (name.equals(prop.getValue("application.user"))
							|| name.equals(prop.getValue("application.userlocal")))
					&& (password != null && (prop.getValue("application.pass").equals(password))
							|| (prop.getValue("application.passlocal")).equals(password))) {
				logger.info(name);
				logger.info(password);
				login = name;
				userGroups = new ArrayList<String>();
				userGroups.add(prop.getValue("application.role"));
			} else {
				loginValid = false;
				throw new LoginException(Constants.Authentication_failed);
			}
		} catch (Exception e) {
			logger.error(Constants.Authentication_failed, e);
			throw new LoginException(Constants.Authentication_failed);
		}
		return loginValid;
	}

	@Override
	public boolean commit() throws LoginException {
		userPrincipal = new JAASUserPrincipal(login);
		subject.getPrincipals().add(userPrincipal);
		if (userGroups != null && userGroups.size() > 0) {
			for (String groupName : userGroups) {
				rolePrincipal = new JAASRolePrincipal(groupName);
				subject.getPrincipals().add(rolePrincipal);
			}
		}
		return true;
	}

	@Override
	public boolean abort() throws LoginException {
		return false;
	}

	@Override
	public boolean logout() throws LoginException {
		subject.getPrincipals().remove(userPrincipal);
		subject.getPrincipals().remove(rolePrincipal);
		return true;
	}

}
